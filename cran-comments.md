## Test environments

* Debian Linux
  - R-devel, GCC (via https://builder.r-hub.io/)
* Windows :
  - R-3.5.1 (local machine)
  - R-4.0.3 (via https://win-builder.r-project.org/)
  - R-devel (via https://win-builder.r-project.org/)
* macOS 10.13.6 High Sierra
  - R-4.0.3, CRAN's setup (via https://builder.r-hub.io/)

## R CMD check results

0 errors | 0 warnings | 0 notes
